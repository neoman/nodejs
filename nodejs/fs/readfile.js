//异步
const fs = require("fs");
fs.readFile('test.txt',function(err,data){
    if(err) throw err;
    console.log(data.toString());
});
//同步
var data = fs.readFileSync('test.txt');
console.log('>>>:' + data);