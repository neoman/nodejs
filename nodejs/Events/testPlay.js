var util = require('util');
var event = require('events');

function Play(){
    event.call(this);
}
util.inherits(Play,event);

var play = new Play();
play.on('pause',function(){
    console.log('pause....');
})
play.on('play',function(){
    console.log('play...');
})
play.emit('play');
play.emit('play2');