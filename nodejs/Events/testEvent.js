const EventEmitter = require('events');
var util = require('util');
class MyEmitter extends EventEmitter{}
const myEmitter = new MyEmitter();
myEmitter.on('aaa',()=>{
    console.log('aaa event occured!');
});
//捕获异常
process.on('uncaughtException',function(){
    console.log('get error');
});
//本质上，浏览器里的事件是由用户触发，浏览器来调用
//这里没有触发的人，程序主动调用
myEmitter.emit('aaa');
//打印注册的事件
console.log(myEmitter.eventNames());
myEmitter.emit('error',new Error('crash'));